using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Control : MonoBehaviour {
    [SerializeField]
    Ray ray;
    Rigidbody rb;

    public Vector3 tensor;
    public GameObject Camera;
    public Text Coord;
    public Text Timer;
    public Text Status;

    public float M_Force = 0.32f, F_Force = 1f;
    public float t_k = 10f, t_k_flight = 10f;

    public float dist_M1 = 0; //0.0005f;
    public float dist_M3 = 0; //-0.001f;

    public float dist_F1_z1 = 0; //-0.05f;
    public float dist_F3_z1 = 0; //0.1f;

    float distance = 0f, angle = 0f, t_0 = 0f, t_min = 0f, t_max = 0f;

    bool calculate = true;
    int modes = 1;
    Vector3 scale; // = new Vector3(0f, 0f, 0f);

    void Start() {
        rb = GetComponent<Rigidbody>();
        rb.inertiaTensor = tensor;
        scale = new Vector3(rb.transform.localScale.x / 2, rb.transform.localScale.y / 2, rb.transform.localScale.z / 2);
    }

    void FixedUpdate() {
        ray = new Ray(Camera.transform.position + scale, Camera.transform.TransformDirection(Vector3.forward));
        Debug.DrawRay(rb.transform.position, rb.transform.forward * 20f, Color.blue);

        Coord.text = "Coordinates: " + rb.transform.position.ToString("F4");
        Timer.text = "Time: " + (Time.time - t_0).ToString("F1");

        if (modes == 1) {
            if (Input.GetKeyDown("g")) {
                distance = Detect().y;
                angle = Mathf.Deg2Rad * Detect().z;
                // Debug.Log("Angle: " + angle + " | Distance: " + distance + " | a: " + Mathf.Deg2Rad);
                if (distance != 0) {
                    t_0 = Time.time;
                    modes = 2;
                }
            }
        } else if (modes == 2)
            Rotation(angle);
        else if (modes == 3)
            Thrust(distance);

    }

    void Rotation(float angle) {
        int k = 1;
        angle = angle + (angle > Mathf.PI ? -1 : angle < -Mathf.PI ? 1 : 0) * 2 * Mathf.PI;
        k = Sign(angle);

        if (calculate) {
            t_min = t_k / 2 - Mathf.Sqrt(t_k * t_k / 4 - k * angle * tensor.x / M_Force);
            // Debug.Log("Rotate t_min: " + t_min);
            t_max = t_k / 2 + Mathf.Sqrt(t_k * t_k / 4 - k * angle * tensor.x / M_Force);
            // Debug.Log("Rotate t_max: " + t_max);
            calculate = false;
        }

        if (k > 0 && (Time.time - t_0 < t_min)) {
            rb.AddTorque(transform.up * (M_Force + dist_M1), ForceMode.Force);
            Status.text = "Status: +Rotating";
        }
        if (k > 0 && (Time.time - t_0 > t_max) && (Time.time - t_0 < t_k)) {
            rb.AddTorque(-transform.up * (M_Force + dist_M1), ForceMode.Force);
            Status.text = "Status: -Rotating at " + t_max.ToString("F1");
        }

        if ((Time.time - t_0 > t_min) && (Time.time - t_0 < t_max))
            Status.text = "Status: Drifting at " + t_min.ToString("F1");

        if (k < 0 && (Time.time - t_0 < t_min)) {
            rb.AddTorque(-transform.up * (M_Force + dist_M3), ForceMode.Force);
            Status.text = "Status: -Rotating";
        }

        if (k < 0 && (Time.time - t_0 > t_max) && (Time.time - t_0 < t_k)) {
            rb.AddTorque(transform.up * (M_Force + dist_M3), ForceMode.Force);
            Status.text = "Status: +Rotating at " + t_max.ToString("F1");
        }


        if (Time.time - t_0 > t_k) {
            calculate = true;
            modes = 3;
        }

    }

    void Thrust(float distance) {
        if (calculate) {
            t_min = t_k_flight / 2 - Mathf.Sqrt(t_k_flight * t_k_flight / 4 - (distance * rb.mass) / F_Force);
            // Debug.Log("Thrust t_min: " + t_min);
            t_max = t_k_flight / 2 + Mathf.Sqrt(t_k_flight * t_k_flight / 4 - (distance * rb.mass) / F_Force);
            // Debug.Log("Thrust t_max: " + t_max);

            // t_min = 5f;
            // t_max = 10f;

            t_0 = Time.time;
            calculate = false;
        }



        if (Time.time - t_0 < t_min) {
            rb.AddForce(transform.forward * (F_Force + dist_F1_z1), ForceMode.Force);
            Status.text = "Status: Accelerating";
        }

        if ((Time.time - t_0 > t_min) && (Time.time - t_0 < t_max))
            Status.text = "Status: Drifting at " + t_min.ToString("F1");

        if ((Time.time - t_0 > t_max) && (Time.time - t_0 < t_k_flight)) {
            rb.AddForce(-transform.forward * (F_Force + dist_F3_z1), ForceMode.Force);
            Status.text = "Status: Braking at " + t_max.ToString("F1");
        }

        if (Time.time - t_0 > t_k_flight) {
            modes = 1;
            calculate = true;
            float J = (Mathf.Pow((rb.position.x + 0.0094f), 2) + Mathf.Pow((rb.position.z + 0.5100f), 2));
            Status.text = "Status: Connected | Evaluate: " + (100 * (rb.position.z + 0.5100f) / Mathf.Sqrt(J)).ToString("F0");
            Debug.Log(rb.transform.position.ToString("F4"));
        }
    }

    Vector3 Detect() {
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit) && hit.collider.gameObject.tag == "ISS")
            return new Vector3(0f, hit.distance, Camera.transform.localRotation.eulerAngles.y);
        else
            return new Vector3(0f, 0f, 0f);
    }

    int Sign(float arg) {
        if (arg > 0)
            return 1;
        else if (arg < 0)
            return -1;

        return 1;
    }
}

// Clean: (-0.0094, 0.0000, 0.5100)
// Dist1: (0.0386, 0.0000, 0.4974)
// Dist2: (0.0388, 0.0000, 0.5410)
// Dist3: (0.0410, 0.0000, 0.4997)
// Dist4: (0.0398, 0.0000, 0.4974)
