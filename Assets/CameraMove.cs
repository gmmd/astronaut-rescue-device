using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour {
	[SerializeField]
	Ray ray;

	private Vector3 _mousePreveousePos;
	private float _rotationX;
	private float _rotationY;
	public GameObject Camera;
	public Transform Pointer;


	// Start is called before the first frame update
	void Start() {
		_mousePreveousePos = Input.mousePosition;
	}

	// Update is called once per frame
	void FixedUpdate() {
		RaycastHit hit;
		ray = new Ray(Camera.transform.position, Camera.transform.TransformDirection(Vector3.forward));

		if (Physics.Raycast(ray, out hit)) {
			Pointer.position = hit.point;
		} else
			Pointer.position = Camera.transform.position;

		Vector3 _mouseDelta;
		if (Input.GetMouseButtonDown(0)) {
			_mousePreveousePos = Input.mousePosition;
		}

		if (Input.GetMouseButton(0)) {
			_mouseDelta = Input.mousePosition - _mousePreveousePos;
			_mousePreveousePos = Input.mousePosition;

			_rotationX -= _mouseDelta.y * 0.4f;
			_rotationY += _mouseDelta.x * 0.4f;

			Camera.transform.localEulerAngles = new Vector3(_rotationX, _rotationY, 0f);
		}
		Debug.DrawRay(Camera.transform.position, Camera.transform.TransformDirection(Vector3.forward) * 20f, Color.yellow);
	}

}
